#!/bin/sh

input_width=$1
input_height=$2
input_video_fd=$3
input_audio_fd=$4
input_video_fps=$5

echo "$@" 1>&2

xterm -fn 10x20 -e sh -c "ffmpeg \
-ar 44100 -ac 2 -f s16le \
-i pipe:$input_audio_fd \
-f rawvideo -r $input_video_fps \
-s ${input_width}x${input_height} \
-i pipe:$input_video_fd \
-sameq -acodec mp2 -vcodec mpeg4 -y /tmp/qemu.avi; read"
