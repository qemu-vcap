#include <caml/fail.h>
#include <caml/alloc.h>
#include <caml/memory.h>

#include <libswscale/swscale.h>

typedef struct {
    int w, h;
    int out_w, out_h;
    int pitch;
    struct SwsContext *sws;
    int linesize[3];
    int offset[3];
    int outsize;
} State;

static State glob_state;

static void sws_reset (State *s)
{
#ifdef ARCH_BIG_ENDIAN
    int fmt = PIX_FMT_RGB565BE;
#else
    int fmt = PIX_FMT_RGB565LE;
#endif

    if (s->sws) {
        sws_freeContext (s->sws);
        s->sws = NULL;
    }

    s->sws = sws_getContext (s->w, s->h, fmt,
                             s->out_w, s->out_h, PIX_FMT_YUV420P,
                             SWS_BICUBIC,
                             NULL, NULL, NULL);
    if (!s->sws)
        caml_failwith ("sws_getContext failed");
}

static void scale (State *s, uint8_t *in, uint8_t *out)
{
    int ret;
    uint8_t *plane[3];

    plane[0] = out;
    plane[1] = plane[0] + s->offset[1];
    plane[2] = plane[1] + s->offset[2];

    ret = sws_scale (s->sws, &in, &s->pitch,
                     0, s->h, plane, s->linesize);
    if (ret != s->out_h)
        caml_failwith ("sws_scale failed");
}

CAMLprim value ml_scale_reset (value params_v)
{
    CAMLparam1 (params_v);
    State *s = &glob_state;

    s->w = Int_val (Field (params_v, 0));
    s->h = Int_val (Field (params_v, 1));
    s->out_w = Int_val (Field (params_v, 2));
    s->out_h = Int_val (Field (params_v, 3));
    s->pitch = Int_val (Field (params_v, 4));

    s->outsize = s->out_w * s->out_h * 3 / 2;

    s->linesize[0] = s->out_w;
    s->linesize[1] = s->out_w / 2;
    s->linesize[2] = s->out_w / 2;

    s->offset[0] = 0;
    s->offset[1] = s->out_w * s->out_h;
    s->offset[2] = s->offset[1] / 4;

    sws_reset (s);

    CAMLreturn (Val_int (s->outsize));
}

CAMLprim value ml_scale (value in_v, value out_v)
{
    CAMLparam2 (in_v, out_v);
    State *s = &glob_state;

    scale (s, String_val (in_v), String_val (out_v));
    CAMLreturn (Val_unit);
}
