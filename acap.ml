open Format;;

type c =
    { silence : string
    ; audio_running : bool
    }

type encset =
    { freq : int
    ; nchannels : int
    ; audiofmt : int
    }

let fail fmt = Printf.kprintf (fun s -> failwith s) fmt;;
let log = printf;;
let log fmt = Printf.kprintf ignore fmt;;

let r8 s o = Char.code s.[o];;
let r16 s o = let b1 = r8 s o and b2 = r8 s (o+1) in (b1 lsl 8) lor b2;;
let r16s s o = let i = r16 s 0 in i - ((i lor 0x8000) lsl 1);;
let r32 s o =
  let w1 = r16 s o
  and w2 = r16 s (o+2) in
  Int32.logor (Int32.shift_left (Int32.of_int w1) 16) (Int32.of_int w2)
;;
let rint s o = let l = r32 s o in Int32.to_int l;;
let w8 s o i = s.[o] <- Char.chr (i land 255);;
let w16 s o i = w8 s (o+1) i; w8 s o (i lsr 8);;
let wint s o i = w16 s (o+2) i; w16 s o (i asr 16);;
let w32 s o l =
  w16 s (o+2) (Int32.to_int l);
  w16 s o (Int32.to_int (Int32.shift_right l 16));
;;

let encodings t =
  let b = Buffer.create 20 in
  Buffer.add_string b "\002xyy";
  let c = "xxxx" in
  let nencodings = List.fold_left
    (fun n i -> w32 c 0 i; Buffer.add_string b c; succ n)
    0 [-259l]
  in
  let s = Buffer.contents b in
  w16 s 2 nencodings;
  s
;;

let setpixfmt =
  let s = String.create 20 in
  w8 s 0 0;
  w8 s 4 16;
  w8 s 5 2;
  w8 s 6 1;
  w8 s 7 1;
  w16 s 8 31;
  w16 s 10 63;
  w16 s 12 31;
  w8 s 14 11;
  w8 s 15 5;
  w8 s 16 0;
  s
;;

let setaudiofmt ~fmt ~nchannels ~freq =
  let s = String.create 10 in
  w8  s 0 255;
  w8  s 1 001;
  w16 s 2 002;
  w8  s 4 fmt;
  w8  s 5 nchannels;
  w32 s 6 freq;
  s
;;

let useaudio =
  let s = String.create 4 in
  w8  s 0 255;
  w8  s 1 001;
  w16 s 2 000;
  s
;;

let create_context ~w ~h ~encset =
  let bytes_per_frame =
    let sample_size =
      match encset.audiofmt with
      | 0 | 1 -> 1
      | 2 | 3 -> 2
      | 4 | 5 -> 4
      | _ -> fail "invalid audiofmt %d" encset.audiofmt
    in
    let _frame_size = sample_size * encset.nchannels in
    0
  in
  { audio_running = false
  ; silence =
      String.make bytes_per_frame '\000' (* wrong for unsigned *)
  }
;;

let reason pump =
  let n = rint (pump#get 4) 0 in
  let s = pump#get n in
  fail "Protocol failure, reason: `%S'" s;
;;

let rec handshake encset pump =
  let s = pump#get 12 in
  let maj, min =
    try
      let dec a b c = a*100 + b*10 + c in
      Scanf.sscanf s "RFB %1i%1i%1i.%1i%1i%1i\n"
        (fun a b c d e f -> dec a b c, dec d e f)
    with exn ->
      fail "can't parse ProtocolVersion `%S'" s
  in

  if maj < 3 || (maj == 3 && min < 8)
  then fail "Too old of a protocol %d.%d" maj min;

  pump#put"RFB 003.008\n";
  let s = let nsectypes = r8 (pump#get 1) 0 in pump#get nsectypes in
  (try ignore (String.index s '\001')
    with Not_found -> fail "None auth not found");
  pump#put "\001";
  if r32 (pump#get 4) 0 <> 0l then reason pump;
  pump#put "\001";
  serverinit encset pump

and serverinit encset pump =
  let s = pump#get 24 in
  let w = r16 s 0
  and h = r16 s 2
  and namelen = rint s 20 in
  let name = pump#get namelen in
  let () =
    printf "server is `%S'@." name;
    pump#put (encodings encset);
    pump#put setpixfmt;
  in
  let context = create_context ~w ~h ~encset in
  boot encset context pump

and boot encset t pump =
  let rec srv1 t =
    pump#run;
    match r8 (pump#get 1) 0 with
    | 000 -> framebuffer_update t
    | 255 -> aliguori t
    | msg -> fail "unexpected server message %x" msg

  and framebuffer_update t =
    let nrects = r16 (pump#get 3) 1 in
    let rec loop t i = if i = nrects then t else
        let s = pump#get 12 in
        let e = r32 s 8 in
        match e with
        | -259l -> loop (audio_ack t) (i+1)
        | _  -> fail "rect with unhandled encoding %lx, %S" e s
    in
    let t = loop t 0 in
    srv1 t

  and audio_ack t =
    let fmt = encset.audiofmt
    and nchannels = encset.nchannels
    and freq = Int32.of_int encset.freq in
    pump#put (setaudiofmt ~fmt ~nchannels ~freq);
    pump#put useaudio;
    t

  and aliguori t =
    match r8 (pump#get 1) 0 with
    | 001 -> audio t
    | msg -> fail "unexpected aliguori message %x" msg

  and audio t =
    match r16 (pump#get 2) 0 with
    | 000 -> srv1 { t with audio_running = false }
    | 001 -> srv1 { t with audio_running = true }
    | 002 -> audio_data t
    | msg -> fail "unexpected audio message %x" msg

  and audio_data t =
    let nbytes = rint (pump#get 4) 0 in
    let audio = pump#get nbytes in
    pump#add_audio audio;
    srv1 t
  in

  srv1 t;
;;

let vncopen addr =
  let sock, addr =
    if String.length addr > 5 && String.sub addr 0 5 = "unix:"
    then
      let path = String.sub addr 5 (String.length addr - 5) in
      let sock = Unix.socket Unix.PF_UNIX Unix.SOCK_STREAM 0 in
      sock, Unix.ADDR_UNIX path
    else
      let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
      let addr, port =
        try
          let p = String.index addr ':' in
          let s = String.sub addr (p + 1) (String.length addr - p - 1) in
          let port =
            try int_of_string s with exn ->
              fail "can't parse port in `%S': %s"
                addr (Printexc.to_string exn)
          in
          let addr = String.sub addr 0 p in
          addr, port
        with Not_found ->
          addr, 5900
      in
      let addr = (Unix.gethostbyname addr).Unix.h_addr_list.(0) in
      sock, Unix.ADDR_INET (addr, port)
  in
  Unix.connect sock addr;
  sock
;;

let pump encset sock =
  let null = Unix.openfile "/dev/null" [] 0 in
  let rapipe, wapipe = Unix.pipe () in
  let () =
    Unix.set_close_on_exec wapipe;
    Unix.set_close_on_exec sock;
    Unix.set_nonblock wapipe;
  in
  let _pid = Unix.create_process
    "/bin/sh"
    [| "/bin/sh"; "./acap.sh"; string_of_int (Obj.magic rapipe) |]
    null
    null
    Unix.stderr
  in
  let rw c f s =
    let rec loop p l =
      let n = f s p l in
      if n = 0 then fail "%s: end of file" c;
      if n = l then () else loop (p + n) (l - n)
    in loop 0 (String.length s)
  in
  let rec write s fd buf pos len () =
    let n =
      try
        let n = Unix.write fd buf pos len in
        if n = 0 then fail "EOF while writing %s" s;
        n
      with Unix.Unix_error (Unix.EAGAIN, _, _) -> 0
    in
    if n = 0 then `again else
      if n = len
      then `completed
      else `more (write s fd buf (pos+n) (len-n))
  in object (self)
    val al = ref []

    method run =
      match !al with
      | [] -> ()
      | l ->
          let _, l, _ = Unix.select [] [wapipe] [] 0.0 in
          let rec loop = function
            | fd :: tl ->
                let rec wrall accu = function
                  | [] -> List.rev accu
                  | f :: tl ->
                      match f () with
                      | `completed -> wrall accu tl
                      | `again -> f :: accu
                      | `more f -> f :: accu
                in
                al := wrall [] !al;
                loop tl
            | [] -> ()
          in
          loop l

    method add_audio data =
      al := !al @ [write "audio" wapipe data 0 (String.length data)]

    method wait dur =
      let r, _, _ = Unix.select [sock] [] [] dur in
      r != []

    method get n = let s = String.create n in rw "get" (Unix.read sock) s; s
    method put s = rw "put" (Unix.write sock) s
  end
;;

let main =
  let host = ref "localhost:5900" in
  Arg.parse (Arg.align [])
    (fun s -> host := s)
    "Usage: acap [host]\n";
  let encset =
    { nchannels = 2
    ; audiofmt = 3
    ; freq = 44100
    }
  in
  try
    let sock = vncopen !host in
    let pump = pump encset sock in
    handshake encset pump;
  with Unix.Unix_error (e, s1, s2) ->
    eprintf "%s(%s): %s@." s1 s2 (Unix.error_message e)
;;
